$(function(){
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});


//スクロールでヘッダー表示
$(document).ready(function(){
	$("#pagetop").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#pagetop').fadeIn();
			} else {
				$('#pagetop').fadeOut();
			}
		});
	});
});

//オーバーレイ表示
$(function() {
     $("#spmenubtn").click(function() {
           $("#overlay").fadeIn();
 });
     $("#spmenubtnclose").click(function() {
           $("#overlay").fadeOut();
 }); 
 
});
